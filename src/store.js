import Vuex from 'vuex'
import Vue from 'vue'


Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        users: [
            {
                id: 1,
                nom: "BOUKARI Marfourz",
                image: require("/public/profil/1.png"),
                connect: true,
                active: false,
                messages: [
                    {
                        id: 2,
                        message: "Peux-tu me faire le design d’une landing page???",
                        heure: "12:10",
                        envoi: false,
                    },
                    {
                        id: 1,
                        vu: true,
                        lu: true,
                        message: "Comment tu vas ? Je serai bientôt la",
                        heure: "12:10",
                        envoi: true,
                    },

                    {
                        id: 3,
                        vu: false,
                        lu: true,
                        message: "Bonsoir madame.J",
                        heure: "12:10",
                        envoi: false,
                    },
                    {
                        id: 4,
                        vu: false,
                        lu: true,
                        message:
                            "Viens me voir au bureau demain à partir de 10h. Mais en attendant, envoie moi ton mail pour que je puisse t’envoyer les détails du produit. Merci pour ta disponibilité.",
                        heure: "12:10",
                        envoi: true,
                    },
                    {
                        id: 5,
                        vu: false,
                        lu: true,
                        message:
                            "Viens me voir au bureau demain à partir de 10h. Mais en attendant, envoie moi ton mail pour que je puisse t’envoyer les détails du produit. Merci pour ta disponibilité.",
                        heure: "12:10",
                        envoi: false,
                    },
                ]
            },
            {
                id: 2,
                nom: "AGOSSOU Jean",
                image: require("/public/profil/2.png"),
                connect: true,
                active: false,
                messages: [
                    {
                        id: 1,
                        vu: true,
                        lu: true,
                        message: "Comment tu vas ? Je serai bientôt la",
                        heure: "12:10",
                        envoi: true,
                    },

                    {
                        id: 3,
                        vu: false,
                        lu: true,
                        message: "Bonsoir madame.J",
                        heure: "12:10",
                        envoi: false,
                    },
                    {
                        id: 4,
                        vu: false,
                        lu: true,
                        message:
                            "Viens me voir au bureau demain à partir de 10h. Mais en attendant, envoie moi ton mail pour que je puisse t’envoyer les détails du produit. Merci pour ta disponibilité.",
                        heure: "12:10",
                        envoi: true,
                    },
                    {
                        id: 5,
                        vu: false,
                        lu: true,
                        message:
                            "Viens me voir au bureau demain à partir de 10h. Mais en attendant, envoie moi ton mail pour que je puisse t’envoyer les détails du produit. Merci pour ta disponibilité.",
                        heure: "12:10",
                        envoi: false,
                    },
                ]
            }
        ],
        currentUser: 0
    },

    mutations: {
        setCurrentUser(state, userId) {
            state.currentUser = userId
        },
    },
    getters: {
        currentUser(state) {
            return state.users.find(user => user.id == state.currentUser)
        },
        currentMessages(state) {
            if (this.currentUser)
                return state.users.find(user => user.id == state.currentUser)
        },
        users(state) {
            return state.users
        }
    }

})



